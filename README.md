# Utiliser des DaemonSets

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Introduction à l'utilisation des DaemonSets

Bonjour et bienvenue dans cette leçon où nous allons parler de l'utilisation des DaemonSets. Voici un aperçu rapide des sujets que nous allons aborder :

1. Qu'est-ce qu'un DaemonSet ?
2. La relation entre les DaemonSets et la planification (scheduling)
3. Une démonstration pratique de la création d'un DaemonSet dans notre cluster Kubernetes

## Qu'est-ce qu'un DaemonSet ?

Un DaemonSet exécute automatiquement une copie d'un pod sur chaque nœud. Par exemple, si nous avons deux nœuds et que nous créons un DaemonSet, nous obtiendrons deux pods, un sur chacun de ces nœuds. Les DaemonSets créent également une copie du pod sur les nouveaux nœuds lorsqu'ils sont ajoutés au cluster. Ainsi, si nous avons un DaemonSet et que nous ajoutons un nouveau nœud, une nouvelle copie du pod du DaemonSet sera automatiquement créée sur ce nouveau nœud.

## La relation entre les DaemonSets et la planification

Il est important de comprendre que les DaemonSets respectent les règles de planification normales concernant les labels des nœuds, les taints et les tolérances. Ainsi, si un pod ne serait normalement pas planifié sur un nœud en raison de ces différents facteurs, le DaemonSet ne créera pas de copie du pod sur ce nœud.

## Démonstration pratique

Passons maintenant à une démonstration pratique pour explorer à quoi ressemblent les DaemonSets dans notre cluster Kubernetes.

1. **Création d'un DaemonSet simple**

   Connectez-vous à votre nœud de contrôle Kubernetes et créez un fichier YAML pour le DaemonSet, nommé `my-daemonset.yml` :

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
    name: my-daemonset
spec:
  selector:
    matchLabels:
      app: my-daemonset
    template:
      metadata:
        labels:
          app: my-daemonset
      spec:
        containers:
        - name: nginx
          image: nginx
```

   Ce DaemonSet va créer un pod Nginx sur chacun de nos nœuds de travail.

2. **Création du DaemonSet**

   Utilisez la commande suivante pour créer le DaemonSet à partir du fichier de description YAML :

```sh
kubectl create -f my-daemonset.yml
```

3. **Vérification du DaemonSet**

   Pour vérifier que le DaemonSet a été créé avec succès, utilisez la commande suivante :

```sh
kubectl get daemonset
```

   Vous devriez voir quelque chose comme ceci :

```plaintext
NAME          DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
my-daemonset  2         2         2       2            2           <none>          1m
```

   Cela signifie que le DaemonSet désire avoir deux pods (un pour chaque nœud de travail), et que ces pods ont été créés et sont prêts.

4. **Vérification des pods**

   Utilisez la commande suivante pour vérifier les pods créés par le DaemonSet :

```sh
kubectl get pods -o wide
```

   Vous verrez les pods créés par le DaemonSet, chacun exécuté sur un nœud de travail différent.

## Conclusion

Pour récapituler, nous avons répondu à la question "Qu'est-ce qu'un DaemonSet ?", parlé de la relation entre les DaemonSets et la planification, et réalisé une démonstration pratique pour montrer comment créer et vérifier un DaemonSet dans un cluster Kubernetes. C'est tout pour cette leçon. À la prochaine !



# Reférences

https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/